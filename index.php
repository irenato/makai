<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 22.01.18
     * Time: 0:50
     */

    get_header();

?>

    <body class="body">
<!-- Custom HTML Start-->
<div class="content index-page">
    <!-- Main-->
    <!-- begin .main-->
    <div class="main">
        <div class="main__center">
            <h1 class="main__heading js_advantages" data-advantages="<?= repeater_data_parse(get_field('main_heading_advantages')) ?>"><?= get_field('main_heading') ?> <em></em></h1>
            <p class="main__descr"><?= get_field('main_description') ?></p>
            <!-- begin .mouse-wrap-->
            <div class="mouse-wrap">
                <!-- begin .mouse--><a href="#work">
                    <div class="mouse"></div>
                    <p class="mouse__p"><?= get_field('button_text') ?></p></a>
                <!-- end .mouse-->
            </div>
            <!-- end .mouse-wrap-->
        </div>
    </div>
    <!-- end .main-->
    <!-- Main-->
    <!-- Begin .content-text-->
    <div class="content-text" id="work">
        <?= str_replace(']]>', ']]&gt;', apply_filters('the_content', get_post(4)->post_content)) ?>
    </div>
    <!-- End .content-text-->
    <!-- Our latest works-->
    <!-- begin .our-works-->
    <div class="our-works">
        <!--h2.our-works__heading Our latest work-->
        <?php $args = [
            'offset'         => 0,
            'posts_per_page' => 5]; ?>
        <?php $posts = new WP_query($args); ?>
        <?php while ($posts->have_posts()) : $posts->the_post(); ?>
            <!-- begin .our-works__wrap-->
            <div class="our-works__wrap js_work_area" data-link="<?= get_the_permalink() ?>">
                <picture class="our-works__img">
                    <source media="(max-width:768px)" srcset="<?= get_field('thumbnail_mobile') ?>">
                    <img class="slider-main__img" src="<?= get_the_post_thumbnail_url() ?>" alt="<?= get_the_title() ?>">
                </picture>
                <div class="our-works__descr">
                    <h2 class="our-works__h2"><?= get_the_title() ?></h2>
                    <p class="our-works__p"><?= get_the_excerpt(); ?></p>
                    <p class="our-works__p">
                        <?php $categories_titles = [];
                            if ($categories = wp_get_post_categories(get_the_ID(), ['fields' => 'all'])) {
                                foreach ($categories as $category):
                                    array_push($categories_titles, $category->name);
                                endforeach;
                                echo implode(', ', $categories_titles);
                            } ?>
                    </p>
                </div>
            </div>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
        <!-- end .our-works__wrap-->
    </div>
    <!-- end .our-works-->
    <!-- Header-->
    <!-- Begin .header-->
    <?php get_template_part('template-parts/menu-header') ?>
    <!-- End .header-->
</div>
<!-- Footer-->
<!-- Begin .footer-->

<?php
    get_footer();


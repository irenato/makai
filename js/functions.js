/**
 * Created by macbook on 18.04.18.
 */
$(document).ready(function () {
    $(".range.ui-widget.ui-widget-content").attr('data-min', $('.range').attr('data-min-default'));
    $(document).on('change', '.js_type', function () {
        var arr = $('.js_type:checked').map(function () {
            return parseInt($(this).data('min'));
        }).get();
        // var max = Math.max.apply(Math, arr);
        var max = arr.reduce(add, 0);
        if(max == Number.POSITIVE_INFINITY || max == Number.NEGATIVE_INFINITY){
            max = parseInt($('#slider-range').data('min'));
        }
        $(".range.ui-widget.ui-widget-content").attr('data-min', "€" + parseInt(max)/1000 + "k");
        $('#slider-range').slider({
            range  : !0,
            animate: 500,
            min    : max,
            max    : parseInt($('#slider-range').data('max')),
            step   : parseInt($('#slider-range').data('step')),
            values : [max, parseInt($('#slider-range').data('default_max'))],
            slide  : function (b, c) {
                $('#amount').val('\u20AC' + c.values[0] / 1e3 + 'k - \u20AC' + c.values[1] / 1e3 + 'k')
            }
        });

        $('#amount').val('\u20AC' + $('#slider-range').slider('values', 0) / 1e3 + 'k - \u20AC' + ($('#slider-range').slider('values', 1) / 1e3 + 'k'));
    })

    function add(a, b) {
        return a + b;
    }
})


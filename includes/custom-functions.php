<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 27.01.18
     * Time: 23:41
     */

    /**
     * @param        $data
     * @param string $key
     * @param string $delimiter
     *
     * @return string
     */
    function repeater_data_parse($data, $key = 'text', $delimiter = '///')
    {
        $res = [];
        if (is_array($data)) {
            foreach ($data as $item) {
                array_push($res, $item[$key]);
            }
        }
        return implode($delimiter, $res);
    }

    /**
     * @param $post_id
     *
     * @return array
     */
    function get_post_categories($post_id)
    {
        $result = [];
        $result['titles'] = [];
        $result['slugs'] = [];
        if ($categories = wp_get_post_categories($post_id, ['fields' => 'all'])) {
            foreach ($categories as $category) {
                array_push($result['titles'], $category->name);
                array_push($result['slugs'], $category->slug);
            }
        }
        return $result;
    }

    function checkCurrent($page_id, $class){
        global $post;
        return $page_id == $post->ID ? $class : '';
    }
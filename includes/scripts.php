<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 22.01.18
     * Time: 0:52
     */

    function makai_styles()
    {
        if (is_front_page()) {
            wp_enqueue_style('makai_styles-index', get_template_directory_uri() . '/css/index.css', '', '', 'all');
        } elseif (is_single()) {
            wp_enqueue_style('makai_styles-slick', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css', '', '', 'all');
            wp_enqueue_style('makai_styles-work', get_template_directory_uri() . '/css/work.css', '', 23123, 'all');
        } elseif (is_page_template('work.php')) {
            wp_enqueue_style('makai_styles-work-main', get_template_directory_uri() . '/css/work-main.css', '', '', 'all');
        } elseif (is_page_template('about.php')) {
            wp_enqueue_style('makai_styles-slick', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css', '', '', 'all');
            wp_enqueue_style('makai_styles-about', get_template_directory_uri() . '/css/about.css', '', '', 'all');
        } elseif (is_page_template('contact.php')) {
            wp_enqueue_style('makai_styles-slick', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css', '', '', 'all');
            wp_enqueue_style('makai_styles-contact', get_template_directory_uri() . '/css/contact.css', '', '', 'all');
        } elseif (is_page_template('planner.php')) {
            wp_enqueue_style('makai_styles-slick', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css', '', '', 'all');
            wp_enqueue_style('makai_styles-project', get_template_directory_uri() . '/css/project.css', '', '', 'all');
        } elseif (is_page_template('studio.php')) {
            wp_enqueue_style('makai_styles-slick', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css', '', '', 'all');
            wp_enqueue_style('makai_styles-studio', get_template_directory_uri() . '/css/studio.css', '', '', 'all');
        } elseif (is_page_template('privacy-policy.php')) {
            wp_enqueue_style('makai_styles-slick', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css', '', '', 'all');
            wp_enqueue_style('makai_styles-privacy-policy', get_template_directory_uri() . '/css/privacy-policy.css', '', '', 'all');
        } else {
            wp_enqueue_style('makai_styles-404', get_template_directory_uri() . '/css/404.css', '', '', 'all');
        }
    }

    add_action('wp_enqueue_scripts', 'makai_styles');

    function makai_scripts()
    {
        if (!is_admin()) {
            wp_deregister_script('jquery');
            wp_register_script('jquery', get_template_directory_uri() . '/js/jquery-2.1.4.min.js');
            wp_enqueue_script('jquery');
        }
        if (is_front_page()) {
            wp_enqueue_script('makai_scripts-typed', '//cdnjs.cloudflare.com/ajax/libs/typed.js/2.0.5/typed.min.js', false, '', true);
            wp_enqueue_script('makai_scripts-index', get_template_directory_uri() . '/js/index.js', false, '', true);
        } elseif (is_single() || is_page_template('studio.php')) {
            wp_enqueue_script('makai_scripts-slick', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', false, '', true);
            wp_enqueue_script('makai_scripts-header', get_template_directory_uri() . '/js/header.js', false, '', true);
        } elseif (is_page_template('work.php')) {
            // wp_enqueue_script('makai_styles-works', get_template_directory_uri() . '/js/works.css', '', '', 'all');
        } elseif (is_page_template('about.php')) {
            wp_enqueue_script('makai_scripts-slick_', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', false, '', true);
            wp_enqueue_script('makai_scripts-about', get_template_directory_uri() . '/js/about.js', false, '', true);
        } elseif (is_page_template('contact.php')) {
            wp_enqueue_script('makai_scripts-slick_', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', false, '', true);
            wp_enqueue_script('makai_scripts-validate', '//cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js', false, '', true);
            wp_enqueue_script('makai_scripts-contact', get_template_directory_uri() . '/js/contact.js', false, '', true);
        } elseif (is_page_template('planner.php')) {
            wp_enqueue_script('makai_scripts-validate', '//cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js', false, '', true);
            wp_enqueue_script('makai_scripts-jquery-ui', get_template_directory_uri() . '/js/jquery-ui.min.js', false, '', true);
            wp_enqueue_script('makai_scripts-touch-punch', '//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js', false, '', true);
            wp_enqueue_script('makai_scripts-project', get_template_directory_uri() . '/js/project.js', false, 453, true);
            wp_enqueue_script('makai_scripts-functions', get_template_directory_uri() . '/js/functions.js', false, 978564, true);
        } elseif (is_page_template('privacy-policy.php')) {
            wp_enqueue_script('makai_scripts-slick_', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', false, '', true);
            wp_enqueue_script('makai_scripts-privacy-policy', get_template_directory_uri() . '/js/privacy-policy.js', false, '', true);
        }
    }

    add_action('wp_enqueue_scripts', 'makai_scripts');
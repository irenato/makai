<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 28.01.18
     * Time: 13:41
     */

    /**
     * contact form
     */
    add_action('wp_ajax_nopriv_contact', 'contact');
    add_action('wp_ajax_contact', 'contact');

    function contact()
    {
        $headers      = 'From: ' . $_POST['email'] . '.' . '<' . $_POST['email'] . '>';
        $to           = get_option('admin_email');
        $mail_subject = $_POST['subject'] . "\r\n";
        $mail_text    = 'Name: ' . $_POST['name'] . "\r\n";
        $mail_text    .= 'Email: ' . $_POST['email'] . "\r\n";
        $mail_text    .= 'Company: ' . $_POST['email'] . "\r\n";
        $mail_text    .= 'Message: ' . $_POST['message'] . "\r\n";
        wp_send_json(wp_mail($to, $mail_subject, $mail_text, $headers));
    }

    /**
     * planner form
     */
    add_action('wp_ajax_nopriv_planner', 'planner');
    add_action('wp_ajax_planner', 'planner');

    function planner()
    {
        $headers      = 'From: ' . $_POST['email'] . '.' . '<' . $_POST['email'] . '>';
        $to           = get_option('admin_email');
        $mail_subject = $_POST['subject'] . "\r\n";
        $mail_text    = 'Name: ' . $_POST['name'] . "\r\n";
        $mail_text    .= 'Email: ' . $_POST['email'] . "\r\n";
        $mail_text    .= 'Company: ' . $_POST['email'] . "\r\n";
        if($_POST['order-type-wrap']){
            $mail_text    .= 'Types: ' . implode(',', $_POST['order-type-wrap']) . "\r\n";
        }
        $mail_text    .= 'Amount: ' . $_POST['amount'] . "\r\n";
        $mail_text    .= 'Message: ' . $_POST['message'] . "\r\n";
        wp_send_json(wp_mail($to, $mail_subject, $mail_text, $headers));
    }
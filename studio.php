<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 28.01.18
     * Time: 15:22
     *
     * Template name: Studio
     */

    get_header();

    if (have_posts()) :
        while (have_posts()) :
            the_post();
            $page_id = $post->ID;
            ?>

            <body class="body">
        <!-- Custom HTML Start-->
        <div class="content studio-page">
            <!-- Header-->
            <!-- Begin .content-->
            <div class="content">
                <?= get_the_content(); ?>
            </div>
            <!-- End .content-->
            <!-- Studio-->
            <!-- begin .studio-->
            <div class="studio">
                <?php if ($items = get_field('types')):
                    foreach ($items as $item): ?>
                        <!-- begin .studio__item-->
                        <div class="studio__item">
                            <div class="studio__main">
                                <img class="studio__img" src="<?= $item['logo'] ?>" alt="<?= $item['title'] ?>">
                                <h2 class="studio__heading"><?= $item['title'] ?></h2>
                                <p class="studio__descr"><?= $item['description'] ?></p>
                            </div>
                            <div class="studio-list">
                                <?php if ($item['items']):
                                    foreach ($item['items'] as $el): ?>
                                        <div class="studio-list__item"><a><?= $el['title'] ?></a></div>
                                    <?php endforeach;
                                endif; ?>
                            </div>
                        </div>
                        <!-- end .studio__item-->
                    <?php endforeach;
                endif;
                ?>
            </div>
            <!-- end .studio-->
            <!-- Studio-->
            <!-- begin .compare-->
            <div class="compare">
                <!-- begin .compare-advantages-->
                <div class="compare-advantages">
                    <h2 class="compare__heading">What we
                        <svg class="heart undefined">
                            <use xlink:href="<?= get_template_directory_uri() ?>/img/symbols.svg#heart"></use>
                        </svg>
                    </h2>
                    <ul class="compare__list">
                        <?php if ($items = get_field('likes')):
                            foreach ($items as $item): ?>
                                <li class="compare__item"><?= $item['title'] ?></li>
                            <?php endforeach;
                        endif; ?>
                    </ul>
                </div>
                <!-- end .compare-advantages-->
                <!-- begin .compare-disadvantages-->
                <div class="compare-disadvantages">
                    <h2 class="compare__heading">What we don’t
                        <svg class="heart heart--rotate">
                            <use xlink:href="<?= get_template_directory_uri() ?>/img/symbols.svg#heart"></use>
                        </svg>
                    </h2>
                    <ul class="compare__list">
                        <?php if ($items = get_field('dislikes')):
                            foreach ($items as $item): ?>
                                <li class="compare__item"><?= $item['title'] ?></li>
                            <?php endforeach;
                        endif; ?>
                    </ul>
                </div>
                <!-- end .compare-disadvantages-->
            </div>
            <!-- end .compare-->
            <!-- Header-->
            <!-- Begin .header-->
            <?php get_template_part('template-parts/menu-header') ?>
            <!-- End .header-->
        </div>
        <!-- Header-->
        <!-- Begin .footer-->

            <?php
        endwhile;
    endif;
    get_footer();

<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 23.01.18
     * Time: 23:46
     *
     *
     * Template name: Contact
     */


    get_header();

    if (have_posts()) :
        while (have_posts()) :
            the_post();
            $page_id = $post->ID;
            ?>

            <body class="body">
            <!-- Custom HTML Start-->
            <div class="content contact-page">
                <!-- Contact-->
                <!-- Begin .contact-->
                <div class="contact">
                    <?= get_the_content() ?>
                    <!-- Begin .contact-form-->
                    <form class="contact-form js_contact_form" data-link="<?= get_the_permalink(359) ?>">
                        <!-- Begin .form-left-->
                        <div class="form-left">
                            <div class="input-wrap">
                                <input class="input" type="text" placeholder="Name" name="name">
                            </div>
                            <div class="input-wrap">
                                <input class="input" type="email" placeholder="Email" name="email">
                            </div>
                            <div class="input-wrap">
                                <input class="input" type="text" placeholder="Company" name="company">
                            </div>
                            <div class="input-wrap">
                                <input class="input" type="text" placeholder="Subject" name="subject">
                            </div>
                        </div>
                        <!-- End .form-left-->
                        <!-- Begin .form-right-->
                        <div class="form-right">
                            <div class="message">
                                <textarea class="message__textarea" placeholder="Message" name="message"></textarea>
                                <div class="form-send">
                                    <button class="form-send__submit" type="submit">
                                        <svg class="paper-plane undefined">
                                            <use xlink:href="<?= get_template_directory_uri() ?>/img/symbols.svg#paper-plane"></use>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!-- End .form-right-->
                    </form>
                    <!-- End .contact-form-->
                </div>
                <!-- End .contact-->
                <!-- Contact-->
                <!-- begin .thanks-->
                <div class="thanks">
                    <h2 class="content__h1 content__h1--golden">Thanks for connecting</h2>
                    <p class="contact__p contact__p--center">We will get back to you as soon as possible!</p>
                </div>
                <!-- end .thanks-->
                <!-- Header-->
                <!-- Begin .header-->
                <?php get_template_part('template-parts/menu-header') ?>
                <!-- End .header-->
            </div>
            <!-- Footer-->
            <!-- Begin .footer-->

            <?php
        endwhile;
    endif;
    get_footer();

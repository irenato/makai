<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 23.01.18
     * Time: 23:45
     */

    get_header();
?>

    <body class="body">
    <!-- Custom HTML Start-->
    <div class="content oops">
        <!-- Main-->
        <!-- begin .main-->
        <div class="main">
            <h1 class="content__h1 content__h1--404 content__h1--small">oops</h1>
            <p class="content-text__p">This page doesn't exist. Maybe you can find what you were looking for in <a href="work.html">our work</a>?
            </p>
        </div>
        <!-- end .main-->
        <!-- Header-->
        <!-- Begin .header-->
        <?php get_template_part('template-parts/menu-header') ?>
        <!-- End .header-->
    </div>

<?php

    get_footer();

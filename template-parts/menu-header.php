<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 28.01.18
     * Time: 1:22
     */

?>

<header class="header">
    <!-- Begin .header-container-->
    <div class="header-container">
        <!-- Begin .header-logo-->
        <a class="header-logo header-logo__img" href="<?= get_home_url() ?>">
            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 190 50">
                <defs>
                    <style>.cls-1, .cls-2 {
                            font-size: 54px;
                            font-family: Lato-Regular, Lato;
                        }

                        .cls-2 {
                            letter-spacing: -0.03em;
                        }</style>
                </defs>
                <title>Makai-Logo-Black</title>
                <text class="cls-1" transform="translate(0 46.35) scale(1.05 1)">M</text>
                <text class="cls-2" transform="translate(54 46.35) scale(1.05 1)">A</text>
                <text class="cls-1" transform="translate(92.83 46.35) scale(1.05 1)">K</text>
                <text class="cls-2" transform="translate(135.33 46.35) scale(1.05 1)">A</text>
                <path d="M190.33,41.85h-4.26a1.9,1.9,0,0,1-1.19-.35,2.17,2.17,0,0,1-.68-.89l-3.81-9.34L168.5,3.15h5.57ZM178.86,27.48l-6.39-15.74q-.28-.7-.6-1.63t-.6-2C170.88,9.58,178.86,27.48,178.86,27.48Z"
                      transform="translate(1 4.5)"/>
            </svg>
        </a>
        <!--img(src="img/logo-185x45.png" alt="logo").header-logo__img-->
        <!-- End .header-logo-->
        <!-- Begin .header-menu-->
        <div class="header-menu">
            <ul class="header-menu__list">
                <li class="header-menu__item <?= checkCurrent(47, 'header-menu__item--active') ?>"><a class="header-menu__link" href="<?= get_the_permalink(47) ?>">Work</a></li>
                <li class="header-menu__item <?= checkCurrent(83, 'header-menu__item--active') ?>"><a class="header-menu__link" href="<?= get_the_permalink(83) ?>">Studio</a></li>
                <li class="header-menu__item <?= checkCurrent(50, 'header-menu__item--active') ?>"><a class="header-menu__link" href="<?= get_the_permalink(50) ?>">About</a></li>
            </ul>
        </div>
        <!-- End .header-menu-->
        <!-- Begin .header-panel-->
        <div class="header-panel">
            <ul class="header-panel__list">
                <li class="header-panel__item"><a class="header-panel__link <?= checkCurrent(67, 'header-panel__link--active') ?>" href="<?= get_the_permalink(67) ?>">contact</a></li>
                <li class="header-panel__item"><a class="header-panel__link <?= checkCurrent(72, 'header-panel__link--active') ?>" href="<?= get_the_permalink(72) ?>">project planner</a></li>
            </ul>
        </div>
        <!-- End .header-panel-->
        <!-- Begin .mobile menu-->
        <!-- Begin .mobile-menu-->
        <div class="mobile-menu"><span></span><span></span><span></span><span></span><span></span><span></span></div>
        <!-- End .mobile-menu-->
        <!-- Begin .mobile-menu-slide-->
        <div class="mobile-menu-slide">
            <div class="mobile-menu-slide__black">
                <ul class="mobile-menu-slide__list">
                    <li class="mobile-menu-slide__item"><a class="mobile-menu-slide__link" href="<?= get_the_permalink(47) ?>">Work</a></li>
                    <li class="mobile-menu-slide__item"><a class="mobile-menu-slide__link" href="<?= get_the_permalink(83) ?>">Studio</a></li>
                    <li class="mobile-menu-slide__item"><a class="mobile-menu-slide__link" href="<?= get_the_permalink(50) ?>">About</a></li>
                </ul>
                <ul class="mobile-menu-slide__nav">
                    <li><a class="mobile-menu-slide__text" href="<?= get_the_permalink(67) ?>">contact</a></li>
                    <li><a class="mobile-menu-slide__text" href="<?= get_the_permalink(72) ?>">project planner</a></li>
                </ul>
            </div>
            <div class="mobile-menu-slide__white"></div>
        </div>
        <!-- End .mobile-menu-slide-->
        <!-- End .mobile menu-->
    </div>
    <!-- End .header-container-->
</header>

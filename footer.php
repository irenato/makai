<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 22.01.18
     * Time: 0:50
     */

    ?>

<footer class="footer">
    <!-- Begin .footer-columns-->
    <div class="footer-columns">
        <!-- Begin .footer-rows 1-->
        <div class="footer-rows">
            <div class="footer-panel footer-panel--ma">
                <ul class="footer-panel__list footer-panel--mobile">
                    <li class="footer-panel__item  <?= checkCurrent(67, 'footer-menu__item--active') ?>"><a class="footer-panel__link" href="<?= get_the_permalink(67) ?>">contact</a></li>
                    <li class="footer-panel__item <?= checkCurrent(72, 'footer-menu__item--active') ?>"><a class="footer-panel__link" href="<?= get_the_permalink(72) ?>">project planner</a></li>
                </ul>
            </div>
        </div>
        <!-- end .footer-rows 1-->
        <!-- Begin .footer-rows 2-->
        <div class="footer-rows">
            <!-- Begin .footer-menu-->
            <div class="footer-menu">
                <ul class="footer-menu__list">
                    <li class="footer-menu__item <?= checkCurrent(47, 'footer-menu__item--active') ?>"><a class="footer-menu__link" href="<?= get_the_permalink(47) ?>">Work</a></li>
                    <li class="footer-menu__item <?= checkCurrent(83, 'footer-menu__item--active') ?>"><a class="footer-menu__link" href="<?= get_the_permalink(83) ?>">Studio</a></li>
                    <li class="footer-menu__item <?= checkCurrent(50, 'footer-menu__item--active') ?>"><a class="footer-menu__link" href="<?= get_the_permalink(50) ?>">About</a></li>
                </ul>
            </div>
            <!-- End .footer-menu-->
            <!-- Begin .footer-panel-->
            <div class="footer-panel footer-panel--bottom">
                <ul class="footer-panel__list">
                    <li class="footer-panel__item footer-panel__item--18px"><a class="footer-panel__link footer-panel__link--18px" href="<?= get_the_permalink(95) ?>">privacy policy / terms</a></li>
                </ul>
            </div>
            <!-- End .footer-panel-->
        </div>
        <!-- End .footer-rows 2-->
        <!-- Begin .footer-rows 3-->
        <div class="footer-rows">
            <p class="footer-rows__p footer-rows--mobile"><span>© <?= date('Y') ?> Makai</span><span class="footer-rows__link"><a class="footer-panel__link footer-panel__link--18px footer-panel__link--mobile"
                                                                                                                           href="<?= get_the_permalink(95) ?>">privacy & terms</a></span></p>
        </div>
    </div>
    <!-- End .footer-columns 3-->
</footer>
<?php wp_footer(); ?>
<!-- End .footer-->
<!-- Custom HTML End-->
<!-- Loading JS Start-->
<!-- Loading JS End-->
<!--<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700|Open+Sans:300,400,600,700" rel="stylesheet">-->
<!--style.-->
<!--  @import url('https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700|Open+Sans:300,400,600,700');-->
<!--script.
WebFontConfig = {
  google: {
    families: ['Lato:300,300i,400,400i,700', 'Open+Sans:300,400,600,700']
  }
};
(function () {
  var wf = document.createElement('script');
  wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
    '://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js';
  wf.type = 'text/javascript';
  wf.async = 'true';
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(wf, s);
})();
-->
</body>
</html>

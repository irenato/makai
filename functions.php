<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 28.07.17
     * Time: 0:15
     */

    include('includes/scripts.php');
    include('includes/custom-functions.php');
    include('includes/ajax.php');

    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );

    add_theme_support('post-thumbnails');

    function themeslug_theme_customizer($wp_customize)
    {
        $wp_customize->add_section('themeslug_logo_section', [
            'title'       => __('Logo', 'themeslug'),
            'priority'    => 30,
            'description' => 'Upload a logo to replace the default site name and description in the header',
        ]);

        $wp_customize->add_setting('themeslug_logo');
        $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'themeslug_logo', [
            'label'    => __('Logo', 'themeslug'),
            'section'  => 'themeslug_logo_section',
            'settings' => 'themeslug_logo',
        ]));
    }

    /**
     * @param $mimes
     *
     * @return mixed
     * for svg
     */
    function cc_mime_types($mimes)
    {
        $mimes['svg'] = 'image/svg+xml';
        return $mimes;
    }

    add_filter('upload_mimes', 'cc_mime_types');

    /* изменим wp-admin на WP_ADMIN_DIR */
    add_filter('site_url', 'wpadmin_filter', 10, 3);

    function wpadmin_filter($url, $path, $orig_scheme)
    {
        $old       = ["/(wp-admin)/"];
        $admin_dir = WP_ADMIN_DIR;
        $new       = [$admin_dir];
        return preg_replace($old, $new, $url, 1);
    }

    /*wp-admin, ошибка 404*/
    add_action( 'init', 'block_wp_admin' );

    function block_wp_admin() {
        if(strpos($_SERVER['REQUEST_URI'],'wp-admin') != false){
            wp_redirect( home_url().'/404' );
            exit;
        }
    }

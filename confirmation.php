<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 13.02.18
     * Time: 22:08
     *
     * Template name: Confirmation
     */
    get_header();

    if (have_posts()) :
        while (have_posts()) :
            the_post();
            ?>

            <body class="body">
        <!-- Custom HTML Start-->
        <div class="content oops">
            <!-- Main-->
            <!-- begin .main-->
            <div class="main">
                <h2 class="content__h1 content__h1--golden"><?= get_the_title(); ?></h2>
                <?= get_the_content(); ?>
            </div>
            <!-- end .main-->
            <!-- Header-->
            <!-- Begin .header-->
            <?php get_template_part('template-parts/menu-header') ?>
            <!-- End .header-->
        </div>

            <?php

        endwhile;
    endif;
    get_footer();

<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 28.01.18
     * Time: 14:09
     *
     * Template name: Project planner
     */

    get_header();

    if (have_posts()) :
        while (have_posts()) :
            the_post();
            $page_id = $post->ID;
            ?>

            <body class="body">
        <!-- Custom HTML Start-->
        <div class="content project">
            <!-- Top text-->
            <!-- Begin .top-text-->
            <div class="top-text">
                <?= get_the_content() ?>
            </div>
            <!-- End .top-text-->
            <!-- Order-->
            <!-- Begin .order-->
            <form class="order" data-link="<?= get_the_permalink(359) ?>">
                <!-- Begin .order-introduction-->
                <div class="order-introduction">
                    <h2 class="order__h2 dot dot--first">
                        <svg class="dot-1 golden">
                            <use xlink:href="<?= get_template_directory_uri() ?>/img/symbols.svg#dot-1"></use>
                        </svg>
                        Introduction
                    </h2>
                    <div class="input-wrap">
                        <input class="input" type="text" name="name" placeholder="Name">
                    </div>
                    <div class="input-wrap">
                        <input class="input" type="text" name="email" placeholder="Email">
                    </div>
                    <div class="input-wrap">
                        <input class="input" type="text" name="phone" placeholder="Phone">
                    </div>
                    <div class="input-wrap">
                        <input class="input" type="text" name="company" placeholder="Company">
                    </div>
                    <div class="input-wrap">
                        <input class="input" type="text" name="subject" placeholder="Subject">
                    </div>
                </div>
                <!-- End .order-introduction-->
                <!-- Begin .order-type-->
                <div class="order-type">
                    <h2 class="order__h2">
                        <svg class="dot-2 golden">
                            <use xlink:href="<?= get_template_directory_uri() ?>/img/symbols.svg#dot-2"></use>
                        </svg>
                        <?= get_field('title_label_1') ?>
                    </h2>
                    <div class="order__p"><?= get_field('description_label_1') ?></div>
                    <div class="order-type-wrap">
                        <?php if ($types = get_field('project_types')):
                            foreach ($types as $type): ?>
                                <div class="checkbox">
                                    <input type="checkbox" class="js_type" name="order-type-wrap[]" value="<?= $type['type'] ?>" data-min="<?= $type['range_min'] ?>" id="<?= $type['type'] ?>">
                                    <label class="order-type__btn" for="<?= $type['type'] ?>"><?= $type['type'] ?></label>
                                </div>
                            <?php endforeach;
                        endif; ?>

                    </div>
                </div>
                <!-- End .order-type-->
                <!-- Begin .order-budget-->
                <div class="order-budget">
                    <h2 class="order__h2">
                        <svg class="dot-3 golden">
                            <use xlink:href="<?= get_template_directory_uri() ?>/img/symbols.svg#dot-3"></use>
                        </svg>
                        <?= get_field('title_label_2') ?>
                    </h2>
                    <p class="order__p"><?= get_field('description_label_2') ?></p>
                    <div class="order-budget__range">
                        <!-- Range-->
                        <!-- Begin .range-->
                        <style>
                            .range.ui-widget.ui-widget-content:before {
                                top: -1.5625rem;
                                left: -7.5rem;
                                content: attr(data-min);
                            }

                            .range.ui-widget.ui-widget-content {
                                /*.range.ui-widget.ui-widget-content:after */
                            }

                            .range.ui-widget.ui-widget-content:after {
                                top: -1.5625rem;
                                right: -7.5rem;
                                content: "€<?= get_field('range_max')/1000 ?>k";
                            }</style>
                        <div class="range" id="slider-range"
                             data-min="<?= get_field('range_min') ?>"
                             data-max="<?= get_field('range_max') ?>"
                             data-step="<?= get_field('range_step') ?>"
                             data-default_min="<?= get_field('range_default_min') ?>"
                             data-default_max="<?= get_field('range_default_max') ?>"
                             data-min-default="€<?= get_field('range_min')/1000 ?>k"
                        ></div>
                        <input class="range__amount" name="amount" type="text" id="amount">
                        <!-- End .range-->
                    </div>
                </div>
                <!-- End .order-budget-->
                <!-- Begin .order-descr-->
                <div class="order-descr">
                    <h2 class="order__h2">
                        <svg class="dot-4 golden">
                            <use xlink:href="<?= get_template_directory_uri() ?>/img/symbols.svg#dot-4"></use>
                        </svg>
                        <span></span> <?= get_field('title_label_3') ?>
                    </h2>
                    <p class="order__p"><?= get_field('description_label_3') ?></p>
                    <div class="order-descr__textarea">
                        <!-- Message-->
                        <!-- Begin .message-->
                        <div class="message">
                            <textarea class="message__textarea" name="message" placeholder="Message"></textarea>
                            <div class="form-send">
                                <button class="form-send__submit" type="submit">
                                    <svg class="paper-plane undefined">
                                        <use xlink:href="<?= get_template_directory_uri() ?>/img/symbols.svg#paper-plane"></use>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        <!-- End .order-descr-->
                    </div>
                </div>
            </form>
            <!-- End .order-->
            <!-- Header-->
            <!-- Begin .header-->
            <?php get_template_part('template-parts/menu-header') ?>
            <!-- End .header-->
            <!-- Contact-->
            <!-- begin .thanks-->
            <div class="thanks">
                <h2 class="content__h1 content__h1--golden">Thank you</h2>
                <p class="contact__p contact__p--center">We will be in touch with you very soon.</p>
            </div>
            <!-- end .thanks-->
        </div>
        <!-- Footer-->
        <!-- Begin .footer-->
            <?php
        endwhile;
    endif;
    get_footer();

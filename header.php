<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="msvalidate.01" content="6BA7B2AB6F7D79EA790C301580C1C80C" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-115979444-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-115979444-1');
    </script>
    <meta charset="utf-8">
    <title><?= wp_get_document_title('|', true, 'right'); ?></title>
    <meta name="<?= get_the_title(); ?>" content="<?= get_the_title(); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="google-site-verification" content="78lUeSd-_xty1WXf3QGPXvbhBb1Om2fvC8Ecwxz-oq0" />
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/site.webmanifest">
    <link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <!-- Custom Browsers Color Start-->
    <!-- Chrome, Firefox OS and Opera-->
    <meta name="theme-color" content="#fff"/>
    <!-- Windows Phone-->
    <meta name="msapplication-navbutton-color" content="#fff"/>
    <!-- iOS Safari-->
    <meta name="apple-mobile-web-app-status-bar-style" content="#fff"/>
    <!-- Custom Browsers Color End-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,600,700" rel="stylesheet">
	<!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script><![endif]-->
	<style>
		@font-face {font-family: 'Halcom-Book';src: url('<?php echo get_stylesheet_directory_uri(); ?>/webfonts/362E7E_0_0.eot');src: url('<?php echo get_stylesheet_directory_uri(); ?>/webfonts/362E7E_0_0.eot?#iefix') format('embedded-opentype'), url('<?php echo get_stylesheet_directory_uri(); ?>/webfonts/362E7E_0_0.woff2') format('woff2'), url('<?php echo get_stylesheet_directory_uri(); ?>/webfonts/362E7E_0_0.woff') format('woff'), url('<?php echo get_stylesheet_directory_uri(); ?>/webfonts/362E7E_0_0.ttf') format('truetype');}
	</style>
    <!-- Loading CSS Start-->
    <?php wp_head(); ?>
    <!-- Loading CSS End-->
</head>


<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 23.01.18
     * Time: 23:45
     */

    get_header();
    if (have_posts()) :
        while (have_posts()) :
            the_post();
            $page_id = $post->ID;
            ?>

            <body class="body">
        <!-- Custom HTML Start-->
        <div class="content work-page">
            <!-- Coco-->
            <!-- begin .coco__img-->
            <picture>
                <source media="(max-width: 480px)" srcset="<?= get_the_post_thumbnail_url() ?>">
                <img class="coco__img" src="<?= get_the_post_thumbnail_url() ?>" alt="coco">
            </picture>
            <!-- end .coco__img-->
            <!-- begin .coco-pink-->
            <div class="coco-pink" style="background-color: <?= get_field('banner_color') ?>">
                <div class="coco-pink__h1"><?= get_field('title_banner') ?></div>
                <div class="coco-pink__p"><?= get_field('description_banner') ?></div>
                <div class="coco-pink__list">
                    <?php if ($categories = wp_get_post_categories($post->ID, ['fields' => 'all'])) {
                        foreach ($categories as $category): ?>
                            <div class="coco-pink__item"><p><?= $category->name ?></p></div>
                        <?php endforeach;
                    } ?>
                </div>
            </div>
            <!-- end .coco-pink-->
            <!-- begin .grid-img-->
            <div class="gallery">
                <?php if ($images = get_field('examples')) {
                    foreach ($images as $image): ?>
                        <?php if ($image['image']): ?>
                            <img class="full-img <?= $image['class'] ?>" alt="img" src="<?= $image['image'] ?>">
                        <?php endif; ?>
                    <?php endforeach;
                }
                ?>
            </div>
            <!-- end .grid-img-->
            <!-- begin .description-->
            <?php if (get_field('description_title')): ?>
                <div class="description">
                    <div class="description__h2">
                        <svg class="dot-1" style="fill: <?= get_field('description_dot_color') ?>">
                            <use xlink:href="<?= get_template_directory_uri() ?>/img/symbols.svg#dot-1"></use>
                        </svg>
                        <?= get_field('description_title') ?>
                    </div>
                    <hr>
                    <div class="description__p"><?= get_field('description') ?></div>
                </div>
            <?php endif; ?>
            <!-- end .description-->
            <!-- begin .full-img-->
            <div class="gallery">
                <?php if ($images = get_field('examples2')) {
                    foreach ($images as $image): ?>
                        <?php if ($image['image']): ?>
                            <img class="full-img <?= $image['class'] ?>" alt="img" src="<?= $image['image'] ?>">
                        <?php endif; ?>
                    <?php endforeach;
                }
                ?>
            </div>
            <?php if (get_field('middle_banner')): ?>
                <picture>
                    <source media="(max-width:480px)" srcset="<?= get_field('middle_banner') ?>">
                    <img class="full-img" src="<?= get_field('middle_banner') ?>" alt="coco">
                </picture>
            <?php endif; ?>
            <!-- end .full-img-->
            <!-- begin .description-->
            <!-- begin .full-img-->
            <?php if (get_field('description_2')): ?>
                <div class="description">
                    <div class="description__h2">
                        <svg class="dot-2" style="fill: <?= get_field('description_2_dot_color') ?>">
                            <use xlink:href="<?= get_template_directory_uri() ?>/img/symbols.svg#dot-2"></use>
                        </svg>
                        <?= get_field('description_2_title') ?>
                    </div>
                    <hr>
                    <div class="description__p"><?= get_field('description_2') ?></div>
                </div>
            <?php endif; ?>
            <!-- end .description-->
            <!-- begin .gallery-->
            <div class="gallery">
                <?php if ($images = get_field('images')) {
                    foreach ($images as $image): ?>
                        <?php if ($image['image']): ?>
                            <img class="full-img <?= $image['class'] ?>" alt="img" src="<?= $image['image'] ?>">
                        <?php endif; ?>
                    <?php endforeach;
                }
                ?>
            </div>
            <!-- end .gallery-->
            <!-- begin .description-->
            <?php if (get_field('description_3')): ?>
                <div class="description">
                    <div class="description__h2">
                        <svg class="dot-3" style="fill: <?= get_field('description_3_dot_color') ?>">
                            <use xlink:href="<?= get_template_directory_uri() ?>/img/symbols.svg#dot-3"></use>
                        </svg>
                        <?= get_field('description_3_title') ?>
                    </div>
                    <hr>
                    <div class="description__p"><?= get_field('description_3') ?></div>
                </div>
            <?php endif; ?>
            <!-- end .description-->
            <!-- begin .gallery-->
            <div class="gallery gallery-last">
                <?php if ($images = get_field('images_2')) {
                    foreach ($images as $image): ?>
                        <?php if ($image['image']): ?>
                            <img class="full-img <?= $image['class'] ?>" alt="img" src="<?= $image['image'] ?>">
                        <?php endif; ?>
                    <?php endforeach;
                }
                ?>
            </div>
            <!-- end .gallery-->
            <!-- begin .blockquote-->
            <blockquote class="blockquote">
                <q class="q">
                    <span class="q__before" style="fill: <?= get_field('quotes_color') ?>">
            <svg class="quotation66 quotation--before">
              <use xlink:href="<?= get_template_directory_uri() ?>/img/symbols.svg#quotation66"></use>
            </svg>
                    </span>
                    <?= get_field('quotation_text') ?>
                    <span class="q__after" style="fill: <?= get_field('quotes_color') ?>">
            <svg class="quotation99 quotation--after">
              <use xlink:href="<?= get_template_directory_uri() ?>/img/symbols.svg#quotation99"></use>
            </svg></span></q>
                <cite><?= get_field('quotation_author') ?></cite>
            </blockquote>
            <!-- end .blockquote-->
            <!-- begin .full-img-->
            <?php if (get_field('bottom_banner')): ?>
                <picture>
                    <source media="(max-width:480px)" srcset="<?= get_field('bottom_banner') ?>">
                    <img class="full-img" src="<?= get_field('bottom_banner') ?>" alt="coco">
                </picture>
            <?php endif; ?>
            <!-- end .full-img-->
            <!-- begin #work.works-->
            <div class="works" id="work">
                <?php $next_post = get_next_post();
                    if (!empty($next_post)): ?>
                <!-- begin .works__item--><a class="works__prev" href="<?= get_permalink($next_post->ID); ?>">
                    <svg class="left-arrow undefined">
                        <use xlink:href="<?= get_template_directory_uri() ?>/img/symbols.svg#left-arrow"></use>
                    </svg>
                    <?php endif; ?>
                </a>
                <div class="works__item">
                    <div class="works__link">visit:<a href="<?= get_field('bottom_link') ?>" target="_blank" rel="nofollow"><?= preg_replace("(^https?://)", "", get_field('bottom_link')) ?></a></div>
                    <p class="works__share"> Share:<a href="https://www.facebook.com/share.php?u=<?= get_permalink() ?>&title=<?= get_the_title() ?>" target="_blank"> Facebook</a> /
                        <a href="https://twitter.com/home?status=<?= get_permalink() . '+' . get_the_title() ?>"
                           target="
_blank"> Twitter</a> /<a href="https://pinterest.com/pin/create/link/?url=<?= get_the_permalink() ?>&media=<?= get_the_post_thumbnail_url() ?>&description=<?= get_the_title() ?>" target="_blank">
                            Pinterest</a> /<a href="mailto:?subject=I%20wanted%20you%20to%20see%20this%20site&amp;body=Check%20out%20this%20site%20<?= get_the_permalink() ?>">
                            Email</a></p>
                </div>
                <?php $next_post = get_previous_post();
                    if (!empty($next_post)): ?>
                <!-- End .works__item--><a class="works__next" href="<?= get_permalink($next_post->ID); ?>">
                    <svg class="right-arrow undefined">
                        <use xlink:href="<?= get_template_directory_uri() ?>/img/symbols.svg#right-arrow"></use>
                    </svg>
                    <?php endif ?>
                </a>
            </div>
            <!-- end #work.works-->
            <!-- Header-->
            <!-- Begin .header-->
            <?php get_template_part('template-parts/menu-header') ?>
            <!-- End .header-->
        </div>
            <?php
        endwhile;
    endif;

    get_footer();

<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 28.01.18
     * Time: 16:08
     *
     * Template name: Privacy policy
     */

    get_header();

    if (have_posts()) :
        while (have_posts()) :
            the_post();
            $page_id = $post->ID;
            ?>

            <body class="body">
            <!-- Custom HTML Start-->
            <div class="content about-page">
                <!-- Content-->
                <!-- Begin .content-->
                <div class="content">
                    <h1 class="content__h1 content__h1--small"><?= get_the_title() ?></h1>
                    <!-- Begin .content-text-->
                    <div class="content-text" id="work">
                       <?= get_the_content(); ?>
                    </div>
                    <!-- End .content-text-->
                </div>
                <!-- End .content-->
                <!-- Header-->
                <!-- Begin .header-->
                <?php get_template_part('template-parts/menu-header') ?>
                <!-- End .header-->
            </div>
            <!-- Footer-->
            <!-- Begin .footer-->

            <?php
        endwhile;
    endif;
    get_footer();

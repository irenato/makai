<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 23.01.18
     * Time: 23:46
     *
     * Template name: About
     */

    get_header();
    if (have_posts()) :
        while (have_posts()) :
            the_post();
            $page_id = $post->ID;
            ?>

            <body class="body">
        <!-- Custom HTML Start-->
        <div class="content about-page">
            <!-- Content-->
            <!-- Begin .content-->
            <div class="content">
                <?php the_content() ?>
            </div>
            <!-- End .content-->
            <!-- Slider-->
            <!-- Begin .slider-->
            <div class="slider">
                <ul class="slider__list" id="main_slider_slides" data-link="<?= get_template_directory_uri() ?>">
                    <?php if ($slider = get_field('slider')):
                        foreach ($slider as $item):?>
                            <li class="slider__item">
                                <picture>
                                    <source media="(max-width:768px)" srcset="<?= $item['image_middle'] ?>">
                                    <img class="slider-main__img" src="<?= $item['image'] ?>" alt="<?= $item['title'] ?>">
                                </picture>
                                <div class="shadow-show shadow-show--visible">
                                    <div class="shadow-show__p js-more"><?= $item['title'] ?>
                                        <!-- svg.plus.js-more
                                        use(xlink:href='./img/symbols.svg#plus')

                                        -->
                                    </div>
                                    <div class="shadow-show-button js-more">
                                        <div class="shadow-show-button__btn">
                                            <svg class="plus">
                                                <use xlink:href="<?= get_template_directory_uri() ?>/img/symbols.svg#plus"></use>
                                            </svg>
                                        </div>
                                        <div class="shadow-show-button__text">Read more</div>
                                    </div>
                                </div>
                                <div class="shadow">
                                    <div class="shadow-wrap">
                                        <div class="shadow__text">
                                            <?= $item['description'] ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="shadow-show shadow-show--hidden">
                                    <div class="shadow-show-button js-less">
                                        <div class="shadow-show-button__btn">
                                            <svg class="minus-in-a-circle">
                                                <use xlink:href="<?= get_template_directory_uri() ?>/img/symbols.svg#minus-in-a-circle"></use>
                                            </svg>
                                        </div>
                                        <div class="shadow-show-button__text">Close</div>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach;
                    endif; ?>
                </ul>
            </div>
            <!-- End .slider-->
            <div class="slider-arrow">
                <svg class="left-arrow-under">
                    <use xlink:href="<?= get_template_directory_uri() ?>/img/symbols.svg#left-arrow-under"></use>
                </svg>
                <svg class="right-arrow-under">
                    <use xlink:href="<?= get_template_directory_uri() ?>/img/symbols.svg#right-arrow-under"></use>
                </svg>
            </div>
            <!-- Begin .cycle-slider-->
            <div class="cycle-slider">
                <ul class="cycle-slider__list" id="slider-nav">
                    <?php if ($slider = get_field('slider')):
                        foreach ($slider as $item):?>
                            <li class="cycle-slider__item"><img src="<?= $item['image_min'] ?>" alt="<?= $item['title'] ?>"></li>
                        <?php endforeach;
                    endif; ?>
                </ul>
            </div>
            <!-- End .cycle-slider-->
            <!-- Blocks-->
            <!-- Begin .blocks-->
            <div class="blocks">
                <h2 class="blocks__h2"><?= get_field('team_title') ?></h2>
                <p class="blocks__p"><?= get_field('team_description') ?></p>
                <!-- Begin .blocks-item-->
                <div class="blocks-items">
                    <?php if ($team = get_field('team')):
                        $i = 0;
                        foreach ($team as $item): ?>
                            <!-- Begin .blocks-block-->
                            <div class="blocks-block">
                                <div class="img-fade-<?= ++$i ?> img-fade">

                                    <?php foreach ($item['images'] as $image): ?>
                                        <img class="blocks-block__img" src="<?= $image['image'] ?>" alt="<?= $item['name'] ?>">
                                    <?php endforeach; ?>
                                </div>
                                <div class="blocks-block__h3"><?= $item['name'] ?></div>
                                <div class="blocks-block__p"><?= $item['description'] ?>
                                </div>

                            </div>
                            <!-- End .blocks-block-->
                        <?php endforeach;
                    endif; ?>
                </div>
                <!-- end .blocks-item-->
                <a class="blocks__btn" href="<?= get_the_permalink(47) ?>"><span>View our work</span></a>
            </div>
            <!-- End .blocks-->
            <!-- Header-->
            <!-- Begin .header-->
            <?php get_template_part('template-parts/menu-header') ?>
            <!-- End .header-->
        </div>
        <!-- Footer-->
        <!-- Begin .footer-->


            <?php

        endwhile;
    endif;

    get_footer();
